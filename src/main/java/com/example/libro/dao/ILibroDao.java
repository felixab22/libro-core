package com.example.libro.dao;

import com.example.libro.entity.Libro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ILibroDao extends JpaRepository<Libro,Long> {

}
