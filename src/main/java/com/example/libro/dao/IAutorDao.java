package com.example.libro.dao;

import com.example.libro.entity.Autor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IAutorDao extends JpaRepository<Autor, Long> {

    @Query("SELECT a FROM Autor a WHERE a.nombre LIKE CONCAT('%',:abr,'%') ")
    List<Autor> getAutorCoinc(@Param("abr") String abr);
}
