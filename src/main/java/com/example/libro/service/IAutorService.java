package com.example.libro.service;

import com.example.libro.entity.Autor;

import java.util.List;

public interface IAutorService {

    Autor saveAutor(Autor autor);

    List<Autor> getAllAutors();

    List<Autor> getByCoincidecnias(String abr);
}
