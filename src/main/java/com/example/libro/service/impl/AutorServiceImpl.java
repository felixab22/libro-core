package com.example.libro.service.impl;

import com.example.libro.dao.IAutorDao;
import com.example.libro.entity.Autor;
import com.example.libro.service.IAutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutorServiceImpl implements IAutorService {

    @Autowired
    private IAutorDao autorDao;

    @Override
    public Autor saveAutor(Autor autor) {
        return this.autorDao.save(autor);
    }

    @Override
    public List<Autor> getAllAutors() {
        return this.autorDao.findAll();
    }

    @Override
    public List<Autor> getByCoincidecnias(String abr) {
        return this.autorDao.getAutorCoinc(abr);
    }
}
