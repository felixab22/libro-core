package com.example.libro.service.impl;

import com.example.libro.dao.ILibroDao;
import com.example.libro.entity.Libro;
import com.example.libro.service.ILibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibroServiceImpl implements ILibroService {

    @Autowired
    private ILibroDao libroDao;

    @Override
    public Libro saveLibro(Libro libro) {
        return this.libroDao.save(libro);
    }

    @Override
    public List<Libro> getAllLibros() {
        return this.libroDao.findAll();
    }

    @Override
    public void deleteLibro(Long idlibro) {
        this.libroDao.deleteById(idlibro);
    }
}
