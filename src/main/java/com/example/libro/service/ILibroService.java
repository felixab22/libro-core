package com.example.libro.service;

import com.example.libro.entity.Libro;

import java.util.List;

public interface ILibroService {

    Libro saveLibro(Libro libro);

    List<Libro> getAllLibros();

    void deleteLibro(Long idlibro);

   // List<Libro> getAllByAnio(String anio);



}
