package com.example.libro.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "LIBRO")
public class Libro implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDLIBRO", length =11)
    private Long idlibro;

    @Column(name = "TITULO", length = 250, nullable = false)
    private String titulo;

    @Column(name = "DESCRIPCION", length = 500)
    private String descripcion;

    @Column(name = "ANIO", length = 100, nullable = false)
    private String anio;

    @Column(name = "PUBLICADO", nullable = false)
    private boolean publicado;

    @Column(name = "fecha", length = 250)
    private String fecha;

    @ManyToOne
    @JoinColumn(name = "IDAUTOR", nullable = false)
    private Autor idautor;

}
