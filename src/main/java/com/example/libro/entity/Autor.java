package com.example.libro.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "AUTOR")
public class Autor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDAUTOR", length =11)
    private Long idautor;

    @Column(name = "NOMBRE", length = 250, nullable = false)
    private String nombre;

    @Column(name = "GENERO", length = 15, nullable = false)
    private String genero;

    public Autor(Long idautor) {
        this.idautor = idautor;
    }

    public Autor() {
    }
}
