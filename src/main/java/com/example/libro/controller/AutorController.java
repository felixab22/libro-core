package com.example.libro.controller;

import com.example.libro.entity.Autor;
import com.example.libro.entity.Libro;
import com.example.libro.service.IAutorService;
import com.example.libro.util.RestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/autor")
public class AutorController {

    @Autowired
    private IAutorService autorService;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateAutor")
    public RestResponse saveOrUpdateAutor(@RequestBody String autorJson)throws JsonProcessingException {
        this.objectMapper = new ObjectMapper();

        Autor autor = objectMapper.readValue(autorJson, Autor.class);

        this.autorService.saveAutor(autor);

        return new RestResponse(HttpStatus.OK.value(),"Guardado correctamente",autor);
    }

    @GetMapping(value = {"/{abr}","/findAll"})
    public RestResponse getAutor(@PathVariable(required = false) String abr)throws JsonProcessingException {

        if (abr!=null){
            List<Autor> autorListf = this.autorService.getByCoincidecnias(abr);
            return new RestResponse(HttpStatus.OK.value(),"Lista de autores",autorListf);
        }
        List<Autor> autorList = this.autorService.getAllAutors();
        return new RestResponse(HttpStatus.OK.value(),"Lista de autores",autorList);
    }

}
