package com.example.libro.controller;

import com.example.libro.entity.Libro;
import com.example.libro.service.ILibroService;
import com.example.libro.util.RestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/libro")
public class LibroController {

    @Autowired
    private ILibroService libroService;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/saveOrUpdateLibro")
    public RestResponse saveOrUpdateLibro(@RequestBody String libroJson)throws JsonProcessingException {
        this.objectMapper = new ObjectMapper();

        Libro libro = objectMapper.readValue(libroJson, Libro.class);

        this.libroService.saveLibro(libro);

        return new RestResponse(HttpStatus.OK.value(),"Guardado correctamente",libro);
    }

    @GetMapping("/getAll")
    public RestResponse getAllLibros()throws JsonProcessingException {

        List<Libro> libros = this.libroService.getAllLibros();

        return new RestResponse(HttpStatus.OK.value(),"Guardado correctamente",libros);
    }

    @PostMapping("/delete/{idlibro}")
    public RestResponse deleteLibro(@PathVariable Long idlibro){

        try {
            this.libroService.deleteLibro(idlibro);

            return new RestResponse(HttpStatus.OK.value(),"registros eliminados corectamente");
        }
        catch (Exception e){
            return new RestResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Registro no eliminado, problemas en el servidor");
        }
    }
}
